#include "pch.h"
#include "progress.hpp"

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define ID_TIMER 100
#define FRAME_INTERVAL 100
#define WINDOW_WIDTH 500
#define WINDOW_HEIGHT 300

using namespace Gdiplus;

ProgressBar* ProgressBar::progress_bar = NULL;
ULONG_PTR ProgressBar::gdiplus_token = NULL;

void ProgressBar::StaticInit(HINSTANCE hInst)
{
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&gdiplus_token, &gdiplusStartupInput, NULL);

	if (!progress_bar)
		progress_bar = new ProgressBar(hInst);
}

void ProgressBar::StaticExit()
{
	if (progress_bar) {
		delete progress_bar;
		progress_bar = NULL;
	}

	Gdiplus::GdiplusShutdown(gdiplus_token);
}

void ProgressBar::Init()
{
	init_event_ = CreateEvent(NULL, TRUE, FALSE, 0);
	thread_ = CreateThread(NULL, 0, StaticWindowThread, (LPVOID)this, 0, NULL);
}

void ProgressBar::Exit()
{
}

void ProgressBar::LoadImage() {
	wchar_t path[MAX_PATH];
	GetModuleFileName(instance_, path, sizeof(path));
	PathRemoveFileSpec(path);
	PathAppend(path, L"Progress.gif");

	image_ = Image::FromFile(path); if (image_) {
		auto count = image_->GetFrameDimensionsCount();
		std::unique_ptr<GUID> dimensionIDs(new GUID[count]);
		image_->GetFrameDimensionsList(dimensionIDs.get(), count);
		image_frames_ = image_->GetFrameCount(dimensionIDs.get());
	}
}

void ProgressBar::WindowInit() {
	const wchar_t className[] = L"ProgressBar";

	WNDCLASS wc = {};
	wc.lpfnWndProc = StaticWindowProc;
	wc.hInstance = instance_;
	wc.lpszClassName = className;
	RegisterClass(&wc);

	progress_window_ = CreateWindowEx(
		0,                          // Optional window styles.
		className,                  // Window class
		L"Progress",                // Window text
		WS_OVERLAPPED | WS_CAPTION, // Window style

		// Size and position
		CW_USEDEFAULT, CW_USEDEFAULT, WINDOW_WIDTH, WINDOW_HEIGHT,

		NULL,       // Parent window    
		NULL,       // Menu
		instance_,  // Instance handle
		this        // Additional application data
	);

	progress_bar_ = CreateWindowEx(
		0,
		PROGRESS_CLASS,
		NULL,
		WS_CHILD | WS_VISIBLE,
		25,
		200,
		430,
		30,
		progress_window_,
		0,
		instance_,
		NULL
	);

	SetTimer(progress_window_, ID_TIMER, FRAME_INTERVAL, NULL);

	LoadImage();

	SetEvent(init_event_);
}

DWORD ProgressBar::WindowPump() {
	MSG message;
	while (GetMessage(&message, NULL, 0, 0)) {
		TranslateMessage(&message);
		DispatchMessage(&message);
	}

	return 0;
}

LRESULT ProgressBar::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_ERASEBKGND:
		return 1;

	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hwnd, &ps);

		HDC hdcMem = CreateCompatibleDC(hdc);
		HBITMAP hbmMem = CreateCompatibleBitmap(hdc, WINDOW_WIDTH, WINDOW_HEIGHT);
		HANDLE hOld = SelectObject(hdcMem, hbmMem);

		FillRect(hdcMem, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW + 1));
		Graphics graphics(hdcMem);
		graphics.DrawImage(image_, 115, 20, 250, 150);

		BitBlt(hdc, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, hdcMem, 0, 0, SRCCOPY);

		SelectObject(hdcMem, hOld);
		DeleteObject(hbmMem);
		DeleteDC(hdcMem);

		EndPaint(hwnd, &ps);
		return 0;
	}

	case WM_TIMER:
	switch (wParam) {
	case ID_TIMER:
	{
		if (image_ && image_frames_) {
			GUID Guid = FrameDimensionTime;
			image_->SelectActiveFrame(&Guid, image_current_frame_);
			image_current_frame_ = ++image_current_frame_ % image_frames_;
			SetTimer(progress_window_, ID_TIMER, FRAME_INTERVAL, NULL);
			InvalidateRect(hwnd, NULL, FALSE);
		}
		return 0;
	}
	}

	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
}

void ProgressBar::Show(int min, int max)
{
	WaitForSingleObject(init_event_, INFINITE);
	ShowWindow(progress_window_, SW_SHOWDEFAULT);
	SendMessage(progress_bar_, PBM_SETRANGE, 0, MAKELPARAM(min, max));
	SendMessage(progress_bar_, PBM_SETPOS, (WPARAM)0, 0);
}

void ProgressBar::Hide()
{
	WaitForSingleObject(init_event_, INFINITE);
	ShowWindow(progress_window_, SW_HIDE);
}

void ProgressBar::SetProgress(int value) {
	WaitForSingleObject(init_event_, INFINITE);
	SendMessage(progress_bar_, PBM_SETPOS, (WPARAM)value, 0);
}

DWORD WINAPI ProgressBar::StaticWindowThread(LPVOID self) {
	ProgressBar* progress = (ProgressBar*)self;
	progress->WindowInit();
	return progress->WindowPump();
}

LRESULT CALLBACK ProgressBar::StaticWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (uMsg == WM_NCCREATE) {
		SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)((CREATESTRUCT*)lParam)->lpCreateParams);
		SetWindowPos(hwnd, 0, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER);
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	ProgressBar* self = (ProgressBar*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
	if (self) return self->WindowProc(hwnd, uMsg, wParam, lParam);
	else return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void __stdcall Show(int min, int max)
{
	ProgressBar::progress_bar->Show(min, max);
}

void __stdcall SetProgress(int value)
{
	ProgressBar::progress_bar->SetProgress(value);
}

void __stdcall Hide()
{
	ProgressBar::progress_bar->Hide();
}
