#pragma once

class ProgressBar {
public:
	ProgressBar(HINSTANCE hInst)
		: instance_(hInst), image_current_frame_(0) {
		Init();
	}

	~ProgressBar() { Exit(); }

	void Show(int min, int max);
	void Hide();

	void SetProgress(int value);

	static void StaticInit(HINSTANCE hInst);
	static void StaticExit();
	static ProgressBar* progress_bar;
	static ULONG_PTR gdiplus_token;

private:
	void Init();
	void Exit();

	void LoadImage();

	void WindowInit();
	DWORD WindowPump();
	LRESULT WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	static DWORD WINAPI StaticWindowThread(LPVOID self);
	static LRESULT CALLBACK StaticWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	HANDLE thread_;
	HANDLE init_event_;
	HINSTANCE instance_;
	HWND progress_window_;
	HWND progress_bar_;

	Gdiplus::Image* image_;
	int image_current_frame_;
	int image_frames_;
};

extern "C" {
	__declspec(dllexport) void __stdcall Show(int min, int max);
	__declspec(dllexport) void __stdcall SetProgress(int value);
	__declspec(dllexport) void __stdcall Hide();
}
