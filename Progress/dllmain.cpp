#include "pch.h"
#include "progress.hpp"

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ulReason,
                       LPVOID lpReserved
                     )
{
    switch (ulReason)
    {
    case DLL_PROCESS_ATTACH:
        ProgressBar::StaticInit(hModule); break;
    case DLL_PROCESS_DETACH:
        ProgressBar::StaticExit(); break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        break;
    }
    return TRUE;
}
