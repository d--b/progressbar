#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>
#include <cstddef>
#include <commctrl.h>
#include <objidl.h>
#include <gdiplus.h>
#include <shlwapi.h>
#pragma comment (lib,"gdiplus.lib")
#pragma comment (lib,"shlwapi.lib")

// C++
#include <memory>