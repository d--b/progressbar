import os
import ctypes


class ProgressBar:
    def __init__(self):
        library_path = os.path.dirname(os.path.abspath(__file__))
        self.progress_dll = ctypes.WinDLL(os.path.join(library_path, 'progress.dll'))

    def show(self, min, max):
        self.progress_dll.Show(min, max)

    def update(self, value):
        self.progress_dll.SetProgress(value)

    def hide(self):
        self.progress_dll.Hide()
