#include "pch.h"

extern "C" {
	__declspec(dllimport) void __stdcall Show(int min, int max);
	__declspec(dllimport) void __stdcall SetProgress(int value);
	__declspec(dllimport) void __stdcall Hide();
}

INT __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCmdLine, INT nCmdShow)
{
	Show(0, 100);
	SetProgress(50);
	for (;;) Sleep(1000);
	return 0;
}